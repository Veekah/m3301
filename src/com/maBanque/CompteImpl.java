package com.maBanque;

public class CompteImpl implements Compte{ 
	 
 	float solde; 
 	 
 	@Override 
 	public void crediter(float credit) throws Exception{ 
 		if (credit <= 0) throw new Exception("credit ne peut etre <= 0");
        this.solde+=credit;
  	}

	@Override
	public float getSolde() {
		return this.solde;
	}

	@Override
	public float debiter(float debit) throws Exception {
		if(debit < 20) throw new Exception("D�bit pas assez important");  
		else if(debit > 1000) throw new Exception("D�bit trop important");
		this.solde -= debit;
		return solde;
	}

	@Override
	public void setSolde(float solde) throws Exception {
		this.solde = solde;
	}
	
	}
